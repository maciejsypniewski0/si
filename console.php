#!/usr/bin/env php
<?php

include 'vendor/autoload.php';

set_time_limit(0);

$app = new \Symfony\Component\Console\Application();

$app->add(new Command\DoGenetics());
$app->add(new Command\RandomSearch());

$app->run();