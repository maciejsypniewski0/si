<?php

namespace Algorithm\Evaluation;

use Algorithm\Genome;
use Symfony\Component\Process\Process;

class SumoEvaluator implements Evaluator
{
    CONST MAX_PROCESSES = 50;

    private $command;
    private $xml;

    public function __construct($command, $xml)
    {
        $this->command = $command;
        $this->xml = $xml;
    }

    /**
     * @param Genome[] $allGenomes
     * @return Genome[]
     */
    public function evaluate($allGenomes)
    {
        $toProcess = [];
        foreach ($allGenomes as $key => $genome) {
            if ($genome->isDirty()) {
                $toProcess[] = [
                    'from' => $key,
                    'genome' => $genome
                ];
            }
        }

        /** @var Process[] $processes */
        $processes = [];
        for ($i = 0; $i < self::MAX_PROCESSES && $i < count($toProcess); $i++) {
            $this->xml->genomeToXml($toProcess[$i]['genome'], $i);
            $processes[] = new Process(sprintf($this->command, $i));
        }

        foreach ($processes as $process) {
            $process->start();
        }

        $running = true;
        $ended = [];
        while ($running) {
            $running = false;
            foreach ($processes as $key => $process) {
                if ($process->isRunning()) {
                    $running = true;
                } else if (false === in_array($key, $ended)) {
                    $ended[] = $key;

                    $allGenomes[$toProcess[$key]['from']]->setFitness($this->getFitnessFromSumoOutput($process->getOutput()));
                    $allGenomes[$toProcess[$key]['from']]->setIsDirty(false);

                    if (count($processes) < count($toProcess)) {
                        $this->xml->genomeToXml($toProcess[$i]['genome'], $i);
                        $processes[$i] = new Process(sprintf($this->command, $i));
                        $processes[$i]->start();
                        $i++;
                        $running = true;
                    }
                }
                usleep(50);
            }
        }

        return $allGenomes;
    }

    private function getFitnessFromSumoOutput($output)
    {
        preg_match('/Inserted: ([0-9]+)/', $output, $matches);
        $inserted = (int)$matches[1];
        preg_match('/Loaded: ([0-9]+)/', $output, $matches);
        $loaded = empty($matches) ? $inserted : (int)$matches[1];
        preg_match('/Running: ([0-9]+)/', $output, $matches);
        $running = (int)$matches[1];
        preg_match('/Waiting: ([0-9]+)/', $output, $matches);
        $waiting = (int)$matches[1];

//        dump($output);
//        dump([
//            'loaded' => $loaded,
//            'inserted' => $inserted,
//            'running' => $running,
//            'waiting' => $waiting
//        ]);
        return 1 - (($loaded - $inserted + $running + $waiting) / $loaded);
    }
}