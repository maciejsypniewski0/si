<?php

namespace Algorithm\Evaluation;


interface Evaluator
{
    public function evaluate($genomes);
}