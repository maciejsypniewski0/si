<?php

namespace Algorithm\Crossing;

use Algorithm\Genome;

class SinglePointColumnCrossingNonDeleting implements Crossing
{
    public function cross($genomes, $ratio)
    {
        $size = count($genomes);

        $newGenomes = [];
        $i = 0;
        while ($i < $size) {
            $parent1 = $genomes[$i];
            if($i === $size -1 ){
                $newGenomes[] = $parent1;
                $i++;
            } else {
                $key1 = random_int(0, $size - 1);
                $parent1 = $genomes[$key1];
                if (random_int(1, 100) / 100 <= $ratio) {
                    do {
                        $key2 = random_int(0, $size - 1);
                    } while ($key1 === $key2);
                    $parent2 = $genomes[$key2];
                    $newGenomes[] = $this->singleCross($parent1, $parent2);
                    $newGenomes[] = $this->singleCross($parent2, $parent1);
                    $i += 2;
                } else {
                    $newGenomes[] = $parent1;
                    $i++;
                }
            }
        }

        return $newGenomes;
    }

    public function singleCross(Genome $g1, Genome $g2)
    {
        $crossingPoint = random_int(6,9);
        return new Genome(array_merge($g1->getGenes(0, $crossingPoint), $g2->getGenes($crossingPoint, $g2->getSize())));
    }

}