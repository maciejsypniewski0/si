<?php

namespace Algorithm\Crossing;

use Algorithm\Genome;

class SinglePointColumnCrossing implements Crossing
{
    /** @var Genome[] $genomes */
    public function cross($genomes, $ratio)
    {
        $size = count($genomes);

        $newGenomes = [];
        while (!empty($genomes)) {
            $genome = array_pop($genomes);
            if (random_int(1, 100) / 100 <= $ratio && !empty($genomes)) {
                $parent1 = $genome;
                $parent2key = array_rand($genomes);
                $parent2 = $genomes[$parent2key];
                $newGenomes[] = $this->singleCross($parent1, $parent2);
                $newGenomes[] = $this->singleCross($parent2, $parent1);

                unset ($genomes[$parent2key]);
            } else {
                $newGenomes[] = $genome;
            }
        }

        return $newGenomes;
    }

    public function singleCross(Genome $g1, Genome $g2)
    {
        $crossingPoint = 8;
        return new Genome(array_merge($g1->getGenes(0, $crossingPoint), $g2->getGenes($crossingPoint, $g2->getSize())));
    }

}