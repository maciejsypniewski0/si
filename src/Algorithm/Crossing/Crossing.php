<?php

namespace Algorithm\Crossing;

use Algorithm\Genome;

interface Crossing
{
    /**
     * @param Genome[] $genomes
     * @param $ratio
     * @return Genome[]
     */
    public function cross($genomes, $ratio);
}