<?php

namespace Algorithm\Crossing;

class UniformCrossing implements Crossing
{
    public function cross($genomes, $ratio)
    {
        $size = count($genomes);
        foreach ($genomes as $key => $genome) {
            do {
                $key2 = random_int(0, $size - 1);
            } while ($key2 === $key);

            $genome2 = $genomes[$key2];
            for ($i = 0; $i < $genome->getSize(); $i++) {
                if (random_int(1, 100) / 100 <= $ratio) {
                    $temp = $genome->getGene($i);
                    $genome->setGene($i, $genome2->getGene($i));
                    $genome2->setGene($i, $temp);
                    $genome->setIsDirty(true);
                    $genome2->setIsDirty(true);
                }
            }
        }

        return $genomes;
    }
}