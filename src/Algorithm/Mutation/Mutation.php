<?php

namespace Algorithm\Mutation;

use Algorithm\Genome;

interface Mutation
{
    /**
     * @param Genome[] $genomes
     * @param $ratio
     */
    public function mutate($genomes, $ratio);
}