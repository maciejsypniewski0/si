<?php

namespace Algorithm\Mutation;


class SimpleMutation implements Mutation
{
    public function mutate($genomes, $ratio)
    {
        $newGenomes = [];
        foreach ($genomes as $genome) {
            if (random_int(1, 100) / 100 <= $ratio) {
                $genome = $genome->randomizeGenes();
            }
            $newGenomes[] = $genome;
        }

        return $newGenomes;
    }

}