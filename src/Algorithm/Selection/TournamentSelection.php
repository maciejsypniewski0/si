<?php

namespace Algorithm\Selection;


class TournamentSelection implements Selection
{
    private $size;

    public function __construct($size)
    {
        $this->size = $size;
    }

    function doSelection($genomes)
    {
        $newGenomes = [];
        $count = count($genomes);
        for ($i = 0; $i < $count; $i++) {
            $keys = $this->randomArray($this->size, $count - 1);

            $max = 0;
            $maxKey = 0;
            foreach ($keys as $key) {
                if($genomes[$key]->getFitness() > $max) {
                    $max = $genomes[$key]->getFitness();
                    $maxKey = $key;
                }
            }
            $newGenomes[] = $genomes[$maxKey];
        }

        return $newGenomes;
    }

    private function randomArray($size, $max)
    {
        $arr = array_fill(0, $size, 0);
        do {
            foreach (range(0, $size - 1) as $r) {
                $arr = array_map(function ($el) use ($max) {
                    return random_int(0, $max);
                }, $arr);
            }
        } while (!$this->arrayIsRandom($arr));
        return $arr;
    }

    private function arrayIsRandom($arr)
    {
        $arraySize = count($arr);
        for ($i = 0; $i < $arraySize - 1; $i++)
            for ($j = $i + 1; $j < $arraySize; $j++)
                if ($arr[$i] === $arr[$j]) return false;
        return true;

    }
}