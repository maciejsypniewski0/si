<?php

namespace Algorithm\Selection;

use Algorithm\Genome;

interface Selection
{
    /**
     * @param \Algorithm\Genome[] $genomes
     * @return Genome[]
     */
    function doSelection($genomes);
}