<?php

namespace Algorithm\Selection;

use Helpers\Random;

class RouletteSelection implements Selection
{
    function doSelection($genomes)
    {
        $sum = 0;
        foreach ($genomes as $genome) {
            $sum += $genome->getFitness();
        }

        $randomfloat = Random::float(0, $sum);
        foreach (range(0, 50) as $r) {
            dump(Random::float(0, $sum));
        }
        die();
    }
}