<?php

namespace Algorithm;


class Genome
{
    /**
     * @var Gene[]
     */
    private $genes;
    private $fitness;
    /** @var  boolean */
    private $isDirty = true;

    public function __construct($genes)
    {
        $this->genes = $genes;
    }

    public function setRandomGeneRandomValue()
    {
        $this->genes[random_int(0, $this->getSize() - 1)]->setRandomValue();
    }

    public function getGene($position)
    {
        return $this->genes[$position];
    }

    public function setGene($position, $gene)
    {
        $this->genes[$position] = $gene;
    }

    public function getGenes($from, $to)
    {
        return array_slice($this->genes, $from, $to - $from);
    }

    public function getSize()
    {
        return count($this->genes);
    }

    /**
     * @return mixed
     */
    public function getFitness()
    {
        return $this->fitness;
    }

    /**
     * @param mixed $fitness
     * @return Genome
     */
    public function setFitness($fitness)
    {
        $this->fitness = $fitness;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDirty(): bool
    {
        return $this->isDirty;
    }

    /**
     * @param bool $isDirty
     * @return Genome
     */
    public function setIsDirty(bool $isDirty): Genome
    {
        $this->isDirty = $isDirty;
        return $this;
    }

    public function randomizeGenes()
    {
        $newGenes = [];
        foreach ($this->genes as $gene) {
            $newGenes[] = $gene->getRandomCopy();
        }
        $genome = new Genome($newGenes);
        $genome->setIsDirty(true);

        return $genome;
    }

    public function newValues($values)
    {
        if (count($values) !== count($this->genes)) {
            throw new \InvalidArgumentException();
        }

        $newGenes = [];
        foreach ($this->genes as $key => $gene) {
            $newGene = $gene->getRandomCopy();
            $newGene->setValue($values[$key]);
            $newGenes[] = $newGene;
        }

        return new Genome($newGenes);
    }
}