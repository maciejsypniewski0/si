<?php

namespace Algorithm;

class Gene
{
    private $tlId;
    private $position;
    private $min;
    private $max;
    private $value;


    public function __construct($tlId, $position, $min, $max, $value)
    {
        $this->tlId = $tlId;
        $this->position = $position;
        $this->min = $min;
        $this->max = $max;
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getTlId()
    {
        return $this->tlId;
    }

    /**
     * @param mixed $tlId
     * @return Gene
     */
    public function setTlId($tlId)
    {
        $this->tlId = $tlId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     * @return Gene
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * @param mixed $min
     * @return Gene
     */
    public function setMin($min)
    {
        $this->min = $min;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * @param mixed $max
     * @return Gene
     */
    public function setMax($max)
    {
        $this->max = $max;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return Gene
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    public function setRandomValue()
    {
        $this->value = random_int($this->getMin(), $this->getMax());
        return $this;
    }

    public function getRandomCopy()
    {
        return new Gene($this->getTlId(), $this->getPosition(), $this->getMin(), $this->getMax(), random_int($this->getMin(), $this->getMax()));
    }
}