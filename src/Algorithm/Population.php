<?php

namespace Algorithm;

use Algorithm\Crossing\Crossing;
use Algorithm\Crossing\SinglePointColumnCrossing;
use Algorithm\Crossing\SinglePointColumnCrossingNonDeleting;
use Algorithm\Crossing\UniformCrossing;
use Algorithm\Evaluation\Evaluator;
use Algorithm\Mutation\Mutation;
use Algorithm\Mutation\SimpleMutation;
use Algorithm\Selection\RouletteSelection;
use Algorithm\Selection\Selection;
use Algorithm\Selection\TournamentSelection;

class Population
{
    /** @var  Genome[] */
    private $genomes;
    /** @var  int */
    private $size;
    /** @var Evaluator */
    private $evaluator;
    private $results;
    /** @var  Crossing */
    private $crosser;
    /** @var Mutation */
    private $mutator;
    /** @var  Selection */
    private $selector;
    /** @var  int */
    private $iteration;

    public function __construct(Genome $initialGenome, int $size, Evaluator $evaluator)
    {
        for ($i = 0; $i < $size; $i++) {
            $genomes[] = $initialGenome->randomizeGenes();
        }

        $this->size = $size;
        $this->iteration = 1;
        $this->evaluator = $evaluator;
        $this->genomes = $genomes;
        $this->crosser = new SinglePointColumnCrossingNonDeleting();
        $this->mutator = new SimpleMutation();
        $this->selector = new TournamentSelection(2);
    }

    public function evaluate()
    {
        $this->genomes = $this->evaluator->evaluate($this->genomes);
        $this->saveResults();
    }

    public function doIteration()
    {
        $this->evaluate();
        $this->doSelection();
        $this->doCrossing();
        $this->doMutation();
        $this->iteration++;
    }

    private function doSelection()
    {
        $this->genomes = $this->selector->doSelection($this->genomes);
    }

    private function doCrossing()
    {
        $this->genomes = $this->crosser->cross($this->genomes, 0.7);
    }

    private function doMutation()
    {
        $this->genomes = $this->mutator->mutate($this->genomes, 0.1);
    }

    private function saveResults()
    {
        $this->results[$this->iteration] = [
            'iteration' => $this->iteration,
            'max' => $this->findMaxFitness(),
            'min' => $this->findMinFitness(),
            'avg' => $this->findAvgFitness()
        ];
        dump($this->results[$this->iteration]);
    }

    private function findMaxFitness()
    {
        $max = 0;
        foreach ($this->genomes as $genome) {
            if ($genome->getFitness() > $max) {
                $max = $genome->getFitness();
            }
        }

        return $max;
    }

    private function findMinFitness()
    {
        $min = 1;
        foreach ($this->genomes as $genome) {
            if ($genome->getFitness() < $min) {
                $min = $genome->getFitness();
            }
        }

        return $min;
    }

    private function findAvgFitness()
    {
        $sum = 0;
        foreach ($this->genomes as $key => $genome) {
            $sum += $genome->getFitness();
        }

        return $sum / $this->size;
    }

    public function getResults()
    {
        return $this->results;
    }
}