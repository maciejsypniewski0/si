<?php

namespace Helpers;


class Random
{
    static function float($min, $max)
    {
        return ($min + lcg_value() * (abs($max - $min)));
    }
}