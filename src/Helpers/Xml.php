<?php

namespace Helpers;

use Algorithm\Gene;
use Algorithm\Genome;
use Symfony\Component\Filesystem\Filesystem;

class Xml
{
    const TLS_CONFIG_FILE = '/';

    private $dataDir;
    private $simulationName;
    private $baseTlsConfig;

    public function __construct($dataDir, $simulationName)
    {
        $this->dataDir = $dataDir;
        $this->simulationName = $simulationName;
        $this->baseTlsConfig =  sprintf('%s/%s/%s_tls.add.xml', $dataDir, $simulationName, $simulationName);
    }

    public function genomeFromXml()
    {
        $genes = [];
        $xml = new \DOMDocument();
        $xml->load($this->baseTlsConfig);
        $tLogic = $xml->getElementsByTagName('tlLogic');
        foreach ($tLogic as $tL) {
            /** @var \DOMNode $tL */
            /** @var \DOMNamedNodeMap $attributes */
            $attributes = $tL->attributes;

            $id = $attributes->getNamedItem('id')->nodeValue;
            foreach ($tL->childNodes as $key => $phase) {
                /** @var \DOMNode $phase */
                if($phase->attributes) {
                    $phaseMin = $phase->attributes->getNamedItem('minDur');
                    $phaseMax = $phase->attributes->getNamedItem('maxDur');
                    if (!$phaseMin || $phaseMin->nodeValue === $phaseMax->nodeValue) continue;

                    $gene = new Gene($id, $key, (int)$phaseMin->nodeValue, (int)$phaseMax->nodeValue, 0);
                    $gene->setRandomValue();

                    $genes[] = $gene;
                }
            }

        }

        return new Genome($genes);
    }

    /**
     * @param Genome $genome
     * @param $key
     */
    public function genomeToXml($genome, $key)
    {
        $copyDir = $this->getSimulationDirCopy($key);
        $tlsConfig = sprintf('%s/%s_tls.add.xml', $copyDir, $this->simulationName);
        $xml = new \DOMDocument();
        $xml->load($tlsConfig);
        $tLogic = $xml->getElementsByTagName('tlLogic');
        foreach($genome->getGenes(0, $genome->getSize()) as $gene) {
            /** @var Gene $gene */
            $tlid = $gene->getTlId();
            foreach ($tLogic as $tL) {
                if($tL->attributes->getNamedItem('id')->nodeValue === $tlid) {
                    foreach ($tL->childNodes as $key => $phase) {
                        if($key === $gene->getPosition()) {
                            $phase->attributes->getNamedItem('duration')->nodeValue = $gene->getValue();
                        }
                    }
                }
            }
        }
        $xml->save($tlsConfig);
    }

    private function getSimulationDirCopy($key)
    {
        $simulationDir = sprintf('%s/%s', $this->dataDir, $this->simulationName);
        $copyDir = $simulationDir . '_' . $key;
        if(!file_exists($copyDir)) {
            $fileSystem = new Filesystem();
            $fileSystem->mkdir($copyDir);

            $directoryIterator = new \RecursiveDirectoryIterator($simulationDir, \RecursiveDirectoryIterator::SKIP_DOTS);
            $iterator = new \RecursiveIteratorIterator($directoryIterator, \RecursiveIteratorIterator::SELF_FIRST);
            foreach ($iterator as $item)
            {
                if ($item->isDir())
                {
                    $fileSystem->mkdir($copyDir . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
                }
                else
                {
                    $fileSystem->copy($item, $copyDir . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
                }
            }
        }

        return $copyDir;
    }
}