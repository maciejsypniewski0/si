<?php

namespace Helpers;


class Excel
{
    private $data;
    /** @var \PHPExcel  */
    private $document;

    public function __construct($data)
    {
        $this->data = $data;
        $this->document = new \PHPExcel();
    }

    public function saveFile($filename)
    {
        $this->document->getActiveSheet()->fromArray(array_keys($this->data[1]), null, 'A1');

        $this->document->getActiveSheet()->fromArray($this->data, null, 'A2');


        $objWriter = \PHPExcel_IOFactory::createWriter($this->document, 'Excel2007');
        ob_start();
        $objWriter->save('php://output');
        file_put_contents(__DIR__ . '/../../results/' . $filename, ob_get_clean());
    }
}