<?php

namespace Command;

use Algorithm\Evaluation\SumoEvaluator;
use Algorithm\Population;
use Helpers\Excel;
use Helpers\Xml;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class DoGenetics extends Command
{
    public function configure()
    {
        $this->setName('si1:dogenetics')
            ->setDescription('Algorytm genetyczny')
            ->addArgument('simname', InputArgument::REQUIRED, '')
            ->addOption('resultsFile', '-r', InputOption::VALUE_OPTIONAL)
            ->addOption('populationSize', '-p', InputOption::VALUE_OPTIONAL, '', 50)
            ->addOption('ffe', '-f', InputOption::VALUE_OPTIONAL, '', 10);

    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $path = __DIR__ . '/../../lib/sumo/bin/sumo.exe';
        $dataPath = __DIR__ . '/../../lib/dane_symulacji';
        $simName = $input->getArgument('simname');
        $resultsFile = $input->getOption('resultsFile');
        $populationSize = $input->getOption('populationSize');
        $ffe = $input->getOption('ffe');

        $cfg = sprintf('%s/%s_%%d/run.sumo.cfg', $dataPath, $simName);
        $xml = new Xml($dataPath, $simName);
        $fullCommand = sprintf('%s -c %s -v', $path, $cfg);

        $initialGenome = $xml->genomeFromXml();
        $evaluator = new SumoEvaluator($fullCommand, $xml);
        $population = new Population($initialGenome, $populationSize, $evaluator);
        $bar = new ProgressBar($output, $ffe);
        $bar->start();
        for ($i = 0; $i < $ffe; $i++) {
            $population->doIteration();
            $bar->advance();
        }
        $bar->finish();

        if($resultsFile){
            $excel = new Excel($population->getResults());
            $excel->saveFile($resultsFile);
        }else{
            dump($population->getResults());
        }
    }
}