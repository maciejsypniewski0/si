<?php

namespace Command;

use Algorithm\Evaluation\SumoEvaluator;
use Algorithm\Genome;
use Algorithm\Population;
use Helpers\Excel;
use Helpers\Xml;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class RandomSearch extends Command
{
    public function configure()
    {
        $this->setName('si1:randomsearch')
            ->setDescription('Losowe wyszukiwanie')
            ->addArgument('simname', InputArgument::REQUIRED, '')
            ->addOption('searches', 's', InputOption::VALUE_OPTIONAL, '', 10000);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $path = __DIR__ . '/../../lib/sumo/bin/sumo.exe --no-step-log';
        $dataPath = __DIR__ . '/../../lib/dane_symulacji';
        $simName = $input->getArgument('simname');
        $searches = $input->getOption('searches');

        $cfg = sprintf('%s/%s_%%d/run.sumo.cfg', $dataPath, $simName);
        $xml = new Xml($dataPath, $simName);
        $fullCommand = sprintf('%s -c %s -v', $path, $cfg);

        $initialGenome = $xml->genomeFromXml();
        $evaluator = new SumoEvaluator($fullCommand, $xml);
        $iterations = ceil($searches / 50);
        $max = 0;
        $bar = new ProgressBar($output, $iterations);
        $bar->start();
        for ($i = 0; $i < $iterations; $i++) {
            $chunk = $this->createChunk($initialGenome);
            $chunk = $evaluator->evaluate($chunk);
            foreach ($chunk as $ch) {
                if ($ch->getFitness() > $max) {
                    $max = $ch->getFitness() . PHP_EOL;
                    echo $max;
                }
            }
            $bar->advance();
        }

        $bar->finish();
        echo "Wyszukiwanie skończone po $searches wyszukiwań, max to $max";
    }

    /**
     * @param Genome $genome
     *
     * @return Genome[]
     */
    private function createChunk($genome)
    {
        $chunk = [];
        for ($i = 0; $i < 50; $i++) {
            $chunk[] = $genome->randomizeGenes();
        }

        return $chunk;
    }
}